$( document ).ready(function() {
  var schedule_html = '';
  var participants_html = '';
  $.getJSON('https://sheets.googleapis.com/v4/spreadsheets/1Tm6nkMxbvf940Ie6ihtCI69G6i9gB2fL-aMRS8BQt7U/values/Agenda?key=AIzaSyB7TU0ePqNvWdrxrBdMGs9_HjnNThGP9Ns', function(data) {
    var values = data.values;
    for (var i = 1 ; i < values.length ; i++) {
      schedule_html += '\
      <ul class="list-schedule-item">\
        <li class="schedule-item-time">' + values[i][0] + '</li>\
        <li class="schedule-item-description ' + values[i][3] + '">\
        <p>' + values[i][1] + '</p>\
        <p>' + values[i][2] + '</p>\
        </li>\
      </ul>';
    }
    $('#list-schedule').html(schedule_html);
  });
  $.getJSON('https://sheets.googleapis.com/v4/spreadsheets/1Tm6nkMxbvf940Ie6ihtCI69G6i9gB2fL-aMRS8BQt7U/values/Participantes?key=AIzaSyB7TU0ePqNvWdrxrBdMGs9_HjnNThGP9Ns', function(data) {
    var values = data.values;
    for (var i = 1 ; i < values.length ; i++) {
      participants_html += '\
      <li class="col-md-2 col-sm-3 col-xs-6">\
        <a href="' + values[i][3] + '" target="_blank">\
          <figure>\
            <img src="img/participants/' + values[i][2] + '.jpg" class="img-responsive" alt="' + values[i][0] + '">\
          </figure>\
          <h3>' + values[i][0] + '</h3>\
          <p>' + values[i][1] + '</p>\
        </a>\
      </li>';
    }
    $('#list-participants').html(participants_html);
  });
  window.sr = ScrollReveal({viewOffset: { top: 0, right: 0, bottom: 100, lehostft: 0 }});
  sr.reveal('.scrolleffect', { duration: 1800 });
  sr.reveal('.scrolleffectelement', 150);
  $('.open-menu').click(function() {
    if($(".mobile-menu").hasClass('in'))
    {
        $(".mobile-menu").removeClass('in');
        $(".page-content").removeClass("out");
        $(this).html('<i class="glyphicon glyphicon-menu-hamburger"></i>');
        $("body").css("position", "relative");
    }
    else
    {
       $(".mobile-menu").addClass('in');
       $(".page-content").addClass("out");
       $(this).html('<i class="glyphicon glyphicon-remove"></i>');
       $("body").css("position", "fixed");
    }
  });
});
